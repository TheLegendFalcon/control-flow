#include <iostream>

bool cVowel(char c) {
  return c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' || c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
}

bool cConstant(char c) {
  return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) && !cVowel(c);
}

bool cDigits(char c) {
  return c >= '0' && c <= '9';
}

bool cPunctuation(char c) {
  return  c == '.' || c == ',' || c == ':' || c == '?' || c == '"' || c == '\'' || c == '!' || c == '_' || c == '-' || c == '/';
}

using namespace std;

int main() {
  char chartype;

  
    cout << "Enter a character: ";
    cin >> chartype;

    string message;

    if (cVowel(chartype))
        message = " is a vowel";
    else if (cConstant(chartype))
        message = " is a consonant";
    else if (cDigits(chartype))
        message = " is a digit";
    else if (cPunctuation(chartype))
        message = " is punctuation";
    else
        message = " is unrecognized";

    cout << chartype << message << "\n";
}
